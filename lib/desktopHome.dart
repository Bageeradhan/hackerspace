import 'package:flutter/material.dart';
class DesktopHome extends StatefulWidget {
  const DesktopHome({Key? key}) : super(key: key);

  @override
  _DesktopHomeState createState() => _DesktopHomeState();
}

class _DesktopHomeState extends State<DesktopHome> {
  var menu =['Home','About','Gallery','Projects','Achievements'];
  var colors = [Colors.amber,Colors.black,Colors.white,Colors.blue,Colors.red];
  PageController controller = PageController();

  void _scrollToIndex(int index){
          controller.animateToPage(index, duration: Duration(seconds: 1), curve: Curves.slowMiddle);
        }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child:Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
             Container(
               color: Colors.white,
               child: Row(
                 children: <Widget>[
                  Image(image: AssetImage('images/hasp.png'),height: 50,width: 120,),
                Spacer(),
                Row(
                  children: List.generate(5, (index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(onTap: () {
                        _scrollToIndex(index);
                      }, child: Container(
                        color: Colors.white,
                        child: Text(menu[index],style: TextStyle(color:Colors.black ,fontFamily: 'VT323',fontSize: 20),))),
                    );
                  }),
                ),
                 ],
               ),
             ),
              Expanded(child:PageView(
                scrollDirection: Axis.vertical,
                pageSnapping: false,
                controller: controller,
                children: <Widget>[
                  HomeD(),
                  AboutD(),
                  Container(color: Colors.red,),
                  Container(color: Colors.blue,),
                  Container(color: Colors.indigo,),
                ]
              ))
            ],
          ) ),
        );

  }
}


class HomeD extends StatefulWidget {
  const HomeD({ Key? key }) : super(key: key);

  @override
  _HomeDState createState() => _HomeDState();
}

class _HomeDState extends State<HomeD> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber,
      child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Welcome to',style: TextStyle(fontFamily: 'CourierPrime',fontSize: 40,color: Colors.black),),
               Text('HackerSpace',style: TextStyle(fontFamily: "CourierPrime",fontSize: 60,fontWeight: FontWeight.bold,color: Colors.black),),
               SizedBox(height: 10,),
               Text('HackerSpace is non-profit coding community where anyone passionate about technology can get started on something to learn/create \n Help spread hackathon culture in our college \n Help,motivate and guide each other to create, innovate and showcase their potential',
               style: TextStyle(fontFamily: 'CourierPrime',fontSize: 15,color: Colors.black),),
                SizedBox(height: 20,),
                GestureDetector(
                  onTap: (){},
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Whats happening ?',style: TextStyle(color: Colors.black),),
                    ),
                  ),
                )
          ],
      ),
    );
  }
}

class AboutD extends StatefulWidget {
  const AboutD({ Key? key }) : super(key: key);

  @override
  _AboutDState createState() => _AboutDState();
}

class _AboutDState extends State<AboutD> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(300, 30,300, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:<Widget> [
            Text('What is Hack Club?',style: TextStyle(fontSize: 30,color: Colors.black),),
            Text('The Hack Club is a non-profit global network of young makers and student-led programming clubs where young people create projects and thrive to think big and make an impact with technology around the world.',style: TextStyle(color: Colors.black),),
            Text('What is Hack HackerSpace?',style: TextStyle(fontSize: 30,color: Colors.black),),
            Text('HackerSpace is non-profit coding community where anyone passionate about technology can get started on something to learn/create \n Help spread hackathon culture in our college \n Help,motivate and guide each other to create, innovate and showcase their potential',
               style: TextStyle(fontFamily: 'CourierPrime',fontSize: 15,color: Colors.black),)
          ],
        ),
      ),
      
    );
  }
}

